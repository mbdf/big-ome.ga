module Main exposing (main)

import Url
import Url.Parser as Parser

import Task

import Browser
import Browser.Dom
import Browser.Events
import Browser.Navigation as Nav

import Html exposing (..)
import Html.Attributes exposing (style)


-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = LinkClicked
        , onUrlChange = UrlChanged
        }



-- MODEL


type alias Height = Float

type alias Model =
    { key : Nav.Key
    , page : Maybe Page
    , height : Height
    }

defaultModel : Url.Url -> Nav.Key -> Model
defaultModel url key =
    { key = key
    , page = parse url
    , height = 0
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( defaultModel url key
    , Browser.Dom.getViewport
        |> Task.perform (.viewport >> .height >> WinResized)
    )



-- UPDATE


type Msg = NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | WinResized Height


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model = case msg of
    NoOp -> ( model, Cmd.none )

    LinkClicked urlRequest ->
        case urlRequest of
            Browser.Internal url ->
                ( model, Nav.pushUrl model.key <| Url.toString url )
            Browser.External href ->
                ( model, Nav.load href )

    UrlChanged url ->
        ( { model | page = parse url }, Cmd.none )

    WinResized height ->
        ( { model | height = height }
        , Browser.Dom.getViewport
            |> Task.andThen (.viewport >> \{x, y} -> Browser.Dom.setViewport x y)
            |> Task.perform (\_ -> NoOp)
        )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onResize <| \_ height -> WinResized (toFloat height)



-- ROUTES


type Page = Home | Contact

parse : Url.Url -> Maybe Page
parse =
    Parser.parse
        ( Parser.oneOf
            [ Parser.map Home Parser.top
            , Parser.map Contact (Parser.s "contact")
            ]
        )



-- VIEW


page : Height -> List (Html msg) -> Html msg
page height =
    div
        [ style "background" "red"
        , style "background-clip" "content-box"
        , style "box-sizing" "border-box"
        , style "min-height" (String.fromFloat height ++ "px")
        , style "padding" "5vh 5vw"
        , style "align-items" "center"
        , style "justify-content" "center"
        , style "display" "flex"
        , style "scroll-snap-align" "start"
        ]

view : Model -> Browser.Document Msg
view model =
    { title = "Big Ω"
    , body =
        [ div
            [ style "scroll-snap-type" "y mandatory"
            , style "overflow-y" "scroll"
            , style "height" (String.fromFloat model.height ++ "px")
            ]
            [ page model.height
                [ div
                    [ style "background" "blue"
                    , style "font-size" "20px"
                    ]
                    [ text """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas quis ipsum suspendisse ultrices gravida dictum. Donec enim diam vulputate ut pharetra sit amet. Tincidunt arcu non sodales neque. Ultricies mi eget mauris pharetra et ultrices neque. Semper feugiat nibh sed pulvinar proin gravida hendrerit lectus. Tortor posuere ac ut consequat semper viverra nam libero justo. Quam pellentesque nec nam aliquam sem et tortor consequat. Elementum curabitur vitae nunc sed velit. Nunc aliquet bibendum enim facilisis gravida neque convallis a. Sit amet venenatis urna cursus eget. Ut sem viverra aliquet eget sit.
    """
                    ]
                ]
            , page model.height
                [ div
                    [ style "width" "500px"
                    , style "height" "500px"
                    , style "background" "blue"
                    ]
                    [
                    ]
                ]
            ]
        ]
    }
